#!/bin/sh

# Alias script in .zshrc 
# Runs through these steps https://dragosinc.atlassian.net/wiki/spaces/EN/pages/1088716854/Telemetry+-+Data+Access+Beta
export VAULT_ADDR=https://vault.dragos.services
/usr/local/bin/vault login -method=oidc
# Sorry
sleep 3
vault write ssh/creds/otp-key-role ip=3.17.143.208
ssh bastion-tel2