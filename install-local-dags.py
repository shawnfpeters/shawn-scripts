import glob
import os

base_dag_dirs = glob.glob('/var/opt/releases/dragos-dags-*/base/')
opt_dag_dirs = glob.glob('/var/opt/releases/dragos-dags-*/opt/')
# airflow-webserver container id
container_id = ''

def docker_copy_dags(f, container_id):
  print('Copying %s to %s:/usr/local/airflow/dags' % (f, container_id))
  os.system('docker cp %s %s:/usr/local/airflow/dags' % (f, container_id))

def main():
  if len(base_dag_dirs) > 1:
    exit("Multiple dragos-dags- directories in the releases directory.")
  else:
    [docker_copy_dags(base_dag_dirs[0] + f, container_id) for f in os.listdir(base_dag_dirs[0]) if f[0:10] == "telemetry-" and f[-3:] == ".py"]
    [docker_copy_dags(opt_dag_dirs[0] + f, container_id) for f in os.listdir(opt_dag_dirs[0]) if f[0:10] == "telemetry-" and f[-3:] == ".py"]
  
if __name__ == "__main__":
    # execute only if run as a script
    main()
